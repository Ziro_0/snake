// import pixi, p2 and phaser ce
import 'pixi';
import 'p2';
import * as Phaser from 'phaser-ce';

// import states
import BootState from './states/BootState';
import PreloadState from './states/PreloadState';
import MainMenuState from './states/MainMenuState';
import GameState from './states/GameState';

import ISnakeGameConfig from './ISnakeGameConfig';

/**
 * Main entry game class
 * @export
 * @class Game
 * @extends {Phaser.Game}
 */
export class Game extends Phaser.Game {
    snkConfig: ISnakeGameConfig;
    listenerMapping: any = {};

    /**
     * Creates an instance of Game.
     * @memberof Game
     */
    constructor(width, height) {
        // call parent constructor
        // super(width, height, Phaser.CANVAS, 'game', null);
        super(540, 960, Phaser.CANVAS, 'game', null);

        // add some game states
        this.state.add('BootState', new BootState(this));
        this.state.add('PreloadState', new PreloadState(this));
        this.state.add('MainMenuState', new MainMenuState(this));
        this.state.add('GameState', new GameState(this));
    }

    startGame(config: ISnakeGameConfig) {
        console.log('game has started');
        this.snkConfig = config;
        console.log(this.snkConfig);
        this.state.start('BootState');
    }

    listen(listenValue, cb) {
        this.listenerMapping[listenValue] = cb;
    }

    resurrect() {
        // this.core.respawn();
    }

    showLivesLost(num) {
        
    }

    showExtraPoints(num) {
        
    }

    endGame() {
        this.paused = true;
        setTimeout(() => {
            this.destroy();
        }, 100);
    }
}
