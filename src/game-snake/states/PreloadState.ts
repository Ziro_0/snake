import JsonKey from '../classes/data/JsonKey';

export default class PreloadState {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private game: Phaser.Game;
  
  private preloadBar: Phaser.Image;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this.game = game;
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    this.game.state.start('MainMenuState');
  }

  // ----------------------------------------------------------------------------------------------
  preload(): void {
    this.preloadBar = this.game.add.image(
      this.game.world.centerX,
      this.game.world.centerY,
      'preload_bar');
    this.preloadBar.anchor.set(0.5);

    this.game.load.setPreloadSprite(this.preloadBar);

    // load assets
    this.loadAtlases();
    this.loadJsonFiles();
    this.loadSounds();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private loadJson(key: string): void {
    const path = 'assets/game-snake/json/';
    this.game.load.json(key, `${path}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadJsonFiles(): void {
    [
      'items',
      'tiles',
      'level0',
      'level1',
      'level2',
    ].forEach((key) => {
      this.loadJson(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlas(key: string): void {
    const baseUrl = `assets/game-snake/atlases/${key}`;
    this.game.load.atlas(key, `${baseUrl}.png`, `${baseUrl}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlases(): void {
    [
      JsonKey.SNAKE_ATLAS,
    ].forEach((key) => {
      this.loadAtlas(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSound(key: string): void {
    const baseUrl = `assets/game-snake/sounds/${key}`;
    const mp3Url = `${baseUrl}.mp3`;
    const oggUrl = `${baseUrl}.ogg`;
    const urls = [
      mp3Url,
      oggUrl
    ];

    this.game.load.audio(key, urls);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSounds(): void {
    [
      'collect0',
      'collect1',
      'collect2',
      'crash0',
      'crash1',
      'crash2',
    ].forEach((key) => {
      this.loadSound(key);
    });
  }
}