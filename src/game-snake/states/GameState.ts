import AudioPlayer from '../classes/audio/AudioPlayer';
import ItemEffect from '../classes/data/ItemEffect';
import PlayerData from '../classes/data/PlayerData';
import ISnakeCollideData from '../classes/entities/ISnakeCollideData';
import Item from '../classes/entities/Item';
import ItemSpawner from '../classes/entities/ItemSpawner';
import Snake from '../classes/entities/Snake';
import Grid from '../classes/levels/Grid';
import ItemData from '../classes/levels/ItemData';
import Level from '../classes/levels/Level';
import Tile from '../classes/levels/Tile';
import LifeBar from '../classes/ui/LifeBar';
import MoveButtons from '../classes/ui/MoveButtons';
import { Game } from '../game';
import Listeners from '../Listeners';
import { getProperty, removeTimer } from '../util/util';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _timer: Phaser.Timer;
  
  private _audioPlayer: AudioPlayer;
  private _grid: Grid;
  private _level: Level;

  private _itemsData: ItemData[];

  private _moveButtons: MoveButtons;
  private _lifeBar: LifeBar;

  private _itemSpawner: ItemSpawner;
  private _snake: Snake;

  private _playerData: PlayerData;

  private _boardGroup: Phaser.Group;
  private _uiGroup: Phaser.Group;
  private _lifeBarTween: Phaser.Tween;
  private _snakeDefTimerEvent: Phaser.TimerEvent;
  private _passLevelTimerEvent: Phaser.TimerEvent;
  private _gameSound: Phaser.Sound;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get boardGroup(): Phaser.Group {
    return (this._boardGroup);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.game.stage.backgroundColor = '#000000';
    
    this._playerData = new PlayerData();
    
    this.initTimer();
    this.initGroups();
    this.initItemsData();
    this.initAudioPlayer();
    this.initUi();
    this.initItemSpawner();
    this.initGrid();
    this.initLevel();
    this.loadDefaultLevel();
    this.initSnake();

    this.startGame();
    
    this.listenerCallback(Listeners.READY, this.game);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game>this.game);
  }

  // ----------------------------------------------------------------------------------------------
  get grid(): Grid {
    return (this._grid);
  }

  // ----------------------------------------------------------------------------------------------
  get items(): Item[] {
    return (this._itemSpawner.items);
  }

  // ----------------------------------------------------------------------------------------------
  get itemsData(): ItemData[] {
    return (this._itemsData);
  }

  // ----------------------------------------------------------------------------------------------
  get itemSpawner(): ItemSpawner {
    return (this._itemSpawner);
  }

  // ----------------------------------------------------------------------------------------------
  get level(): Level {
    return (this._level);
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    if (this._lifeBarTween) {
      this._lifeBarTween.stop();
      this._lifeBarTween = null;
    }

    this._snake.destroy();
    this._itemSpawner.destroy();
    this._audioPlayer.dispose();
    
    this._snakeDefTimerEvent = removeTimer(this._snakeDefTimerEvent);
    this._passLevelTimerEvent = removeTimer(this._passLevelTimerEvent);

    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (this.itemSpawner) {
      if (this.itemSpawner.shouldRretrySpawnOnNextFrame) {
        this.itemSpawner.spawn();
      }
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private addItemPoints(): void {
    const points = (this._playerData.level + 1) + (this._snake.length - 1);
    this._playerData.score += points;
    this.listenerCallback(Listeners.ADD_POINT, this._playerData.score);
  }

  // ----------------------------------------------------------------------------------------------
  private collectItemsToPassLevel(): boolean {
    if (!(!!this.level.passItems) || this.level.passItems <= 0) {
      return (false);
    }

    this._playerData.itemsCollectedThisLevel += 1;
    if (this._playerData.itemsCollectedThisLevel < this.level.passItems) {
      return (false);
    }

    this.loadNextLevel();
    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private createLifeBar(): void {
    if (!this._lifeBar) {
      this._lifeBar = new LifeBar(this, 120, 600);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private damageSnake(isSelfCollision: boolean): void {
    if (isSelfCollision) {
      this.listenerCallback(Listeners.SNAKE_HIT_SELF);
    } else {
      this.listenerCallback(Listeners.SNAKE_HIT_BLOCK);
    }

    if (this.gameObject.snkConfig.oneHitDefCollisions) {
      this._snakeDef();
    } else {
      if (this._snake.length === 1) {
        this._snakeDef();
      } else {
        this._snakeHit();
      }
    }
  }

  // ----------------------------------------------------------------------------------------------
  private enableSnakeShield(): void {
    this.createLifeBar();

    const config = this.gameObject.snkConfig;

    this._snake.enableShield(
      config.shieldDuration,
      config.shieldWarningDuration,
      config.warningFlashRateMs,
    );

    if (this._lifeBarTween) {
      this._lifeBarTween.stop();
    }

    this._lifeBar.value = 1.0;

    const totalDuration = config.shieldDuration + config.shieldWarningDuration;
    this._lifeBarTween = this.game.add.tween(this._lifeBar).to(
      {
        value: 0
      },
      totalDuration,
      Phaser.Easing.Linear.None,
      true,
    )

    this._lifeBarTween.onComplete.addOnce(() => {
      this._lifeBar.destroy();
      this._lifeBar = null;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private growSnake(): void {
    this._snake.length += 1;
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initGrid(): void {
    const COLUMNS = 32;
    const ROWS = 56;
    const CELL_SIZE = 20;
    this._grid = new Grid(COLUMNS, ROWS, CELL_SIZE);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._boardGroup = this.game.add.group();
    const headerBarDiv = document.getElementById('headerBar');
    this._boardGroup.y = headerBarDiv.clientHeight * this.game.scale.scaleFactor.y;
    
    this._uiGroup = this.game.add.group();
  }

  // ----------------------------------------------------------------------------------------------
  private initItemsData(): void {
    this._itemsData = [];

    const jData: any[] = this.game.cache.getJSON('items');
    if (!Array.isArray(jData)) {
      console.warn('No items json file, "items.json" found.');
      return;
    }

    jData.forEach((jUnit) => {
      this._itemsData.push(new ItemData(jUnit));
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initItemSpawner(): void {
    this._itemSpawner = new ItemSpawner(this);
  }

  // ----------------------------------------------------------------------------------------------
  private initLevel(): void {
    const GRID_CELL_SIZE = 20;
    this._level = new Level(this, GRID_CELL_SIZE);
  }

  // ----------------------------------------------------------------------------------------------
  private initSnake(): void {
    const config = this.gameObject.snkConfig;
    this._snake = new Snake(
      this,
      config.maxSnakeLength,
      this._level.snakeStartPosition.x,
      this._level.snakeStartPosition.y,
      config.defaultSnakeLength,
    );

    this._snake.onCollided.add(this.onSnakeCollided, this);
    this._snake.onCollectedItem.add(this.onSnakeCollectedItem, this);

    this._snake.direction = this._level.snakeStartDirection;
  }

  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this._moveButtons = new MoveButtons(this, this.onMoveUpPressed,
      this.onMoveDownPressed, this.onMoveLeftPressed, this.onMoveRightPressed, this);

    const keys = this.game.input.keyboard.createCursorKeys();
    keys.up.onDown.add(this.onMoveUpPressed, this);
    keys.down.onDown.add(this.onMoveDownPressed, this);
    keys.left.onDown.add(this.onMoveLeftPressed, this);
    keys.right.onDown.add(this.onMoveRightPressed, this);
  }

  // ----------------------------------------------------------------------------------------------
  private listenerCallback(key: string, ...args): void {
    const callback: Function = this.gameObject.listenerMapping[key];
    if (callback) {
      callback.call(this.game, ...args);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private loadDefaultLevel(): void {
    const startLevel = this.gameObject.snkConfig.startLevel || 0;
    this.loadLevel(startLevel);
  }

  // ----------------------------------------------------------------------------------------------
  private loadLevel(levelIndex: number): void {
    if (!this._level.load(levelIndex, this._boardGroup)) {
      return;
    }

    this._playerData.level = levelIndex;
    this._playerData.itemsCollectedThisLevel = 0;

    this.createLevelPassTimer();
  }

  // ----------------------------------------------------------------------------------------------
  private createLevelPassTimer(): boolean {
    if (!(!!this._level.passTime) || this._level.passTime <= 0) {
      return (false);
    }

    this._passLevelTimerEvent = removeTimer(this._passLevelTimerEvent);
    
    const ms = this._level.passTime * 1000;
    this._passLevelTimerEvent = this.timer.add(ms, () => {
      this.loadNextLevel();
    });

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private loadNextLevel(): void {
    const levelIndex = this._playerData.level + 1;
    if (levelIndex >= this._level.length) {
      return;
    }

    this.loadLevel(levelIndex);
  }

  // ----------------------------------------------------------------------------------------------
  private onMoveDownPressed(): void {
    this._snake.moveDown();
  }

  // ----------------------------------------------------------------------------------------------
  private onMoveLeftPressed(): void {
    this._snake.moveLeft();
  }

  // ----------------------------------------------------------------------------------------------
  private onMoveRightPressed(): void {
    this._snake.moveRight();
  }

  // ----------------------------------------------------------------------------------------------
  private onMoveUpPressed(): void {
    this._snake.moveUp();
  }

  // ----------------------------------------------------------------------------------------------
  private onSnakeCollectedItem(item: Item): void {
    this.playSound(item.itemData.sound);

    this.resolveItemEffects(item);
    this._itemSpawner.collect(item);
    this._itemSpawner.spawn();

    this.collectItemsToPassLevel();
  }

  // ----------------------------------------------------------------------------------------------
  private onSnakeCollided(data: ISnakeCollideData): void {
    if (data.collidedWithSelf) {
      // snake collided with self
      this.damageSnake(true);
      return;
    }

    if (!data.collidedTile || !data.collidedTile.isBlockActive) {
      return;
    }

    // snake collided with an active block tile
    if (this._snake.hasShield) {
      this.shatterTile(data.collidedTile);
    } else {
      this.damageSnake(false);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private playSound(keys: string[]): void {
    this._stopGameSound();

    const key = Phaser.ArrayUtils.getRandomItem(keys);
    if (key) {
      this._gameSound = this.gameObject.sound.play(key);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private resolveItemEffects(item: Item): void {
    item.itemData.effects.forEach((effectId) => {
      switch (effectId) {
        case ItemEffect.GROW:
          this.growSnake();
          break;

        case ItemEffect.KILL:
          this._snakeDef();
          break;

        case ItemEffect.POINTS:
          this.addItemPoints();
          break;

        case ItemEffect.SHIELD:
          this.enableSnakeShield();
          break;
          
        case ItemEffect.SPEED_DOWN:
          this._snake.speed -= 1;
          break;
          
        case ItemEffect.SPEED_UP:
          this._snake.speed += 1;
          break;
          
        default:
          console.log(`Unknown effect id: ${effectId}`);
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  private shatterTile(tile: Tile): void {
    this.listenerCallback(Listeners.SNAKE_SHATTER_BLOCK);
    this.playSound(this.gameObject.snkConfig.blockShatterSounds);
    tile.shatter();
  }

  // ----------------------------------------------------------------------------------------------
  private _snakeDef(): void {
    this.playSound(this.gameObject.snkConfig.snakeDefSounds);

    this._snake.def();

    this._snakeDefTimerEvent = this.timer.add(1500, () => {
      this.listenerCallback(Listeners.LIVES_LOST, this.game);
    }, this);
  }

  // ----------------------------------------------------------------------------------------------
  private _snakeHit(): void {
    this.playSound(this.gameObject.snkConfig.snakeHitSounds);
    this._snake.defSegment();
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    this._snake.start();
    this._itemSpawner.spawn();
  }

  // ----------------------------------------------------------------------------------------------
  private _stopGameSound(): void {
    if (this._gameSound) {
      this._gameSound.stop();
      this._gameSound = null;
    }
  }
}
