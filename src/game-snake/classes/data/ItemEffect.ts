enum ItemEffect {
  GROW = 'grow',
  KILL = 'kill',
  POINTS = 'points',
  SHIELD = 'shield',
  SPEED_DOWN = 'speed-down',
  SPEED_UP = 'speed-up',
}

export default ItemEffect;
