import { AudioEventKey } from './AudioEventKey';

export default class AudioPlayer {
  //=============================================================================================
  // properties
  //=============================================================================================
  private _game: Phaser.Game;
  
  //=============================================================================================
  // public
  //=============================================================================================
  
  //---------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this._game = game;
  }

  //---------------------------------------------------------------------------------------------
  dispose() {
    if (this._game) {
      this._game.sound.removeAll();
      this._game = null;
    }
  }
  
  //---------------------------------------------------------------------------------------------
  play(soundKey: AudioEventKey, stopCallback?: Function, stopCallbackContext?: any): boolean {
    const sound = this._game.sound.play(soundKey);
    if (!sound) {
      console.warn(`AudioPlayer.play. Could not play sound with key ${soundKey}`);
      return(false);
    }

    if (stopCallback) {
      sound.onStop.addOnce(stopCallback, stopCallbackContext);
    }

    return(true);
  }
}
