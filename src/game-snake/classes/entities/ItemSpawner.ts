import GameState from '../../states/GameState';
import { irandomRange, removeTimer } from '../../util/util';
import ItemData from '../levels/ItemData';
import ItemSpawnLocation from '../levels/ItemSpawnLocation';
import Item from './Item';

export default class ItemSpawner {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  gameState: GameState;

  private _items: Item[];

  private spawnTimerEvent: Phaser.TimerEvent;

  private _shouldRretrySpawnOnNextFrame: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // -----------------------------------------------------------------------------------------------
  constructor(gameState: GameState) {
    this.gameState = gameState;
    this._items = [];
  }

  // ----------------------------------------------------------------------------------------------
  collect(item: Item): void {
    this.destroyItem(item);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.destroyAllItems();
    this.destroySpawnTimer();
  }

  // ----------------------------------------------------------------------------------------------
  destroyAllItems(): void {
    this.items.forEach((item) => {
      item.destroy();
    });

    this._items.length = 0;
  }

  // -----------------------------------------------------------------------------------------------
  destroyItem(item: Item): void {
    const index = this._items.indexOf(item);
    if (index > -1) {
      Phaser.ArrayUtils.remove(this._items, index);
      item.destroy();
    }
  }
  
  // ----------------------------------------------------------------------------------------------
  get items(): Item[] {
    return (this._items.concat());
  }

  // ----------------------------------------------------------------------------------------------
  get shouldRretrySpawnOnNextFrame(): boolean {
    return (this._shouldRretrySpawnOnNextFrame);
  }

  // ----------------------------------------------------------------------------------------------
  spawn(): Item {
    if (!this.canSpawnItem()) {
      this._shouldRretrySpawnOnNextFrame = true;
      return (null);
    }

    const location = this.getRandomLocation();
    if (!location) {
      console.warn ('Could not get item spawn location.');
      return (null);
    }

    const itemKey = this._getRandomItemKey(location);
    if (!itemKey) {
      console.warn ('Could not get item key.');
      return (null);
    }

    const itemData = this.gameState.itemsData.find((data) => data.key === itemKey);
    if (!itemData) {
      console.warn (`Item data with key: ${itemKey} not found.`);
      return (null);
    }
    
    const point = this._getItemPosition(location);
    if (!point) {
      console.warn ('Could not get item position.');
      return (null);
    }

    if (this.doesPositionCollideWithBlocks(point)) {
      this._shouldRretrySpawnOnNextFrame = true;
      return (null);
    }

    this._shouldRretrySpawnOnNextFrame = false;

    const item = this.createItem(point.x, point.y, itemData);
    
    this.setupSpawnTimer();
    
    return (item);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private canSpawnItem(): boolean {
    return (this._items.length < this.gameState.level.maxItems);
  }

  // ----------------------------------------------------------------------------------------------
  private createItem(x: number, y: number, data: ItemData): Item {
    const item = new Item(this.gameState, x, y, data);
    
    item.onExpired.addOnce((expiredItem: Item) => {
      this.destroyItem(expiredItem);
      this.spawn();
    });

    this._items.push(item);
    return (item);
  }

  // ----------------------------------------------------------------------------------------------
  private destroySpawnTimer(): void {
    this.spawnTimerEvent = removeTimer(this.spawnTimerEvent);
  }

  // -----------------------------------------------------------------------------------------------
  private doesPositionCollideWithBlocks(position: Phaser.Point): boolean {
    const level = this.gameState.level;
    for (let row = 0; row < level.rows; row += 1) {
      for (let column = 0; column < level.columns; column += 1) {
        const tile = level.getTile(column, row);
        if (tile.isBlock && position.x === column && position.y === row) {
          return (true);
        }
      }
    }

    return (false);
  }

  // -----------------------------------------------------------------------------------------------
  private _getRandomItemKey(location: ItemSpawnLocation): string {
    return (<string>Phaser.ArrayUtils.getRandomItem(location.items));
  }

  // -----------------------------------------------------------------------------------------------
  private getRandomLocation(): ItemSpawnLocation {
    const location = this.gameState.level.getRandomItemSpawnLocation();
    if (!location) {
      return (null);
    }
    return (location);
  }

  // -----------------------------------------------------------------------------------------------
  private _getItemPosition(location: ItemSpawnLocation): Phaser.Point {
    const point = new Phaser.Point(
      irandomRange(location.x, location.x + location.width - 1),
      irandomRange(location.y, location.y + location.height - 1),
    );

    return (point);
  }

  // -----------------------------------------------------------------------------------------------
  private onTimerSpawn(): void {
    if (this.spawn()) {
      return;
    }

    if (this._shouldRretrySpawnOnNextFrame) {
      return;
    }

    if (!this.canSpawnItem()) {
      this._shouldRretrySpawnOnNextFrame = true;
    }
  }

  // -----------------------------------------------------------------------------------------------
  private setupSpawnTimer(): void {
    if (this.spawnTimerEvent) {
      return;
    }

    const rate = this.gameState.level.itemSpawnRate;
    if (rate > 0) {
      this.spawnTimerEvent = this.gameState.timer.loop(rate, this.onTimerSpawn, this);
    }
  }
}