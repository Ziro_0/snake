import GameState from '../../states/GameState';
import JsonKey from '../data/JsonKey';
import ItemData from '../levels/ItemData';
import DurationTimer, { IVisible } from './DurationTimer';

export default class Item implements IVisible {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  gameState: GameState;
  
  onExpired: Phaser.Signal;

  private _image: Phaser.Image;

  private _itemData: ItemData;
  
  private _durationTimer: DurationTimer;

  private _column: number;
  
  private _row: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, startColumn: number, startRow: number, itemData: ItemData) {
    this.gameState = gameState;
    this.onExpired = new Phaser.Signal();
    this._column = startColumn;
    this._row = startRow;
    this._itemData = itemData;
    this.initImage();
    this.initDurationTimer();
  }

  // ----------------------------------------------------------------------------------------------
  get column(): number {
    return (this._column);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.onExpired.dispose();
    this._durationTimer.destroy();
    this.image.destroy();
    this.gameState = null;
    this._itemData = null;
  }

  // ----------------------------------------------------------------------------------------------
  get image(): Phaser.Image {
    return (this._image);
  }

  // ----------------------------------------------------------------------------------------------
  get itemData(): ItemData {
    return (this._itemData);
  }

  // ----------------------------------------------------------------------------------------------
  get row(): number {
    return (this._row);
  }

  // ----------------------------------------------------------------------------------------------
  get visible(): boolean {
    return (this._image.visible);
  }

  // ----------------------------------------------------------------------------------------------
  set visible(value: boolean) {
    this._image.visible = value;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private initDurationTimer(): void {
    this._durationTimer = new DurationTimer(this.gameState, this);
    this._durationTimer.onComplete.add(this.onDurationTimerComplete, this);
    
    const config = this.gameState.gameObject.snkConfig;
    this._durationTimer.start(config.itemDuration, config.itemWarningDuration,
      config.warningFlashRateMs);
  }

  // ----------------------------------------------------------------------------------------------
  private initImage(): void {
    if (!this._itemData || !this._itemData.imageFrame) {
      return;
    }

    const grid = this.gameState.grid;
    this._image = this.gameState.game.add.image(
      grid.toCoor(this.column),
      grid.toCoor(this.row),
      JsonKey.SNAKE_ATLAS,
      this._itemData.imageFrame,
    );

    this.gameState.boardGroup.add(this.image);
  }

  // ----------------------------------------------------------------------------------------------
  private onDurationTimerComplete(): void {
    this.onExpired.dispatch(this);
  }
}
