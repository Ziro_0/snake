import GameState from '../../states/GameState';

// ================================================================================================
export interface IVisible {
  visible: boolean;
}

// ================================================================================================
export default class DurationTimer {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  gameState: GameState;

  onComplete: Phaser.Signal;

  private _durationTimerEvent: Phaser.TimerEvent;

  private _warningFlashTimerEvent: Phaser.TimerEvent;

  private _visibleObject: IVisible;

  private _warningDurationMs: number;

  private _warningFlashRateMs: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, visibleObject: IVisible) {
    this.gameState = gameState;
    this._visibleObject = visibleObject;
    this.onComplete = new Phaser.Signal();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    if (this._visibleObject) {
      this._visibleObject.visible = true;
      this._visibleObject = null;
    }

    this.removeTimers();
  }

  // ----------------------------------------------------------------------------------------------
  start(durationMs: number, warningDurationMs: number, warningFlashRateMs: number): void {
    this._warningDurationMs = warningDurationMs;
    this._warningFlashRateMs = warningFlashRateMs;

    this.removeTimers();

    this._durationTimerEvent = this.gameState.timer.add(durationMs, this.onDurationTimer, this);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private onDurationTimer(): void {
    this._durationTimerEvent = this.gameState.timer.add(this._warningDurationMs,
      this.onWarningTimer, this);

    this._warningFlashTimerEvent = this.gameState.timer.loop(this._warningFlashRateMs,
      this.onWarningFlashTimer, this);
  }

  // ----------------------------------------------------------------------------------------------
  private onWarningFlashTimer(): void {
    this._visibleObject.visible = !this._visibleObject.visible;
  }

  // ----------------------------------------------------------------------------------------------
  private onWarningTimer(): void {
    this._durationTimerEvent = null;
    this._warningFlashTimerEvent = DurationTimer.RemoveTimer(this._warningFlashTimerEvent);
    this._visibleObject.visible = true;
    this.onComplete.dispatch(this);
  }

  // ----------------------------------------------------------------------------------------------
  private static RemoveTimer(event: Phaser.TimerEvent): Phaser.TimerEvent {
    if (event) {
      event.timer.remove(event);
    }

    return (null);
  }

  // ----------------------------------------------------------------------------------------------
  private removeTimers(): void {
    this._durationTimerEvent = DurationTimer.RemoveTimer(this._durationTimerEvent);
    this._warningFlashTimerEvent = DurationTimer.RemoveTimer(this._warningFlashTimerEvent);
  }
}
