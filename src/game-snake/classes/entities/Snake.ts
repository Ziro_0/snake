import GameState from '../../states/GameState';
import Direction from '../data/Direction';
import JsonKey from '../data/JsonKey';
import Tile from '../levels/Tile';
import DurationTimer, { IVisible } from './DurationTimer';
import ISnakeCollideData from './ISnakeCollideData';
import Item from './Item';

// ================================================================================================
interface _ISnakeSegment {
  image?: Phaser.Image;
  column: number;
  row: number;
}

// ================================================================================================
export default class Snake implements IVisible {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  gameState: GameState;

  private _segments: _ISnakeSegment[];

  private _proposedPositions: Phaser.Point[];

  private _prevPositions: Phaser.Point[];

  private _durationTimer: DurationTimer;

  private _direction: Direction;

  private _pendingDirection: Direction;

  private _moveTimerEvent: Phaser.TimerEvent;

  private _defTimerEvent: Phaser.TimerEvent;

  private _onCollided: Phaser.Signal;

  private _onCollectedItem: Phaser.Signal;

  private _maxLength: number;

  private _speed: number;

  private _yallDoneDied: boolean;

  private _hasShield: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, maxLength: number, startColumn: number,
  startRow: number, startLength = 1, startSpeed = 1) {
    this.gameState = gameState;
    
    this._onCollided = new Phaser.Signal();
    this._onCollectedItem = new Phaser.Signal();

    this._direction = Direction.RIGHT;
    this.speed = Math.max(1, startSpeed);

    this.initSegments(startColumn, startRow, startLength);
    this._maxLength = Math.max(this.length, maxLength);

    this._durationTimer = new DurationTimer(this.gameState, this);
    this._durationTimer.onComplete.add(this.onDurationTimerComplete, this);
  }

  // ----------------------------------------------------------------------------------------------
  /**
   * The DEF of the snake... Mwa ha ha! >:)
   */
  def(): void {
    if (this._yallDoneDied) {
      return;
    }

    this._yallDoneDied = true;

    if (this._segments.length === 0) {
      return;
    }
    
    let index = 0;
    this._defTimerEvent = this.gameState.timer.repeat(100, this._segments.length,
      () => {
        const segment = this._segments[index];
        segment.image.play('death');
        index += 1;
      },
      this);
  }

  // ----------------------------------------------------------------------------------------------
  defSegment(): void {
    this.length -= 1;
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this._moveTimerEvent = this.removeTimer(this._moveTimerEvent);
    this._defTimerEvent = this.removeTimer(this._defTimerEvent);
    
    this._durationTimer.destroy();

    this._onCollided.dispose();
    this._onCollectedItem.dispose();
    this.stop();
    this.destroySegments();
    this.gameState = null;
  }
  
  // ----------------------------------------------------------------------------------------------
  set direction(value: Direction) {
    this._direction = value;
    this.setHeadDirection();
  }
  
  // ----------------------------------------------------------------------------------------------
  get direction(): Direction {
    return (this._direction);
  }

  // ----------------------------------------------------------------------------------------------
  enableShield(durationMs: number, warningDurationMs: number, warningFlashRateMs: number): void {
    this._durationTimer.start(durationMs, warningDurationMs, warningFlashRateMs);
    this._hasShield = true;
  }

  // ----------------------------------------------------------------------------------------------
  get hasShield(): boolean {
    return (this._hasShield);
  }

  // ----------------------------------------------------------------------------------------------
  get length(): number {
    return (this._segments.length);
  }

  // -----------------------------------------------------------------------------------------------
  set length(value: number) {
    if (this._yallDoneDied) {
      return;
    }

    if (this.maxLength > 0) {
      value = Math.min(value, this.maxLength);
    }
    
    if (value < 1) {
      return;
    }

    const numValidSegments = this._segments.filter(seg => seg.image).length;
    if (value > numValidSegments) {
      this.grow(value);
    } else if (value < numValidSegments) {
      this.shrink(value);
    }
  }

  // ----------------------------------------------------------------------------------------------
  get maxLength(): number {
    return (this._maxLength);
  }

  // -----------------------------------------------------------------------------------------------
  moveDown(): void {
    if (this.direction !== Direction.UP) {
      this._pendingDirection = Direction.DOWN;
      return;
    }

    const toCol = this._segments[0].column;
    const toRow = this._updatePositionWrapRow(this._segments[0].row + 1);
    
    for (let index = 1; index < this._segments.length; index += 1) {
      const segment = this._segments[1];
      if (segment.column === toCol && segment.row === toRow) {
        return;
      }
    }
    
    this._pendingDirection = Direction.DOWN;
  }

  // -----------------------------------------------------------------------------------------------
  moveLeft(): void {
    if (this.direction !== Direction.RIGHT) {
      this._pendingDirection = Direction.LEFT;
    }
  }

  // -----------------------------------------------------------------------------------------------
  moveRight(): void {
    if (this.direction !== Direction.LEFT) {
      this._pendingDirection = Direction.RIGHT;
    }
  }

  // -----------------------------------------------------------------------------------------------
  moveUp(): void {
    if (this.direction !== Direction.DOWN) {
      this._pendingDirection = Direction.UP;
    }
  }

  // ----------------------------------------------------------------------------------------------
  get onCollectedItem(): Phaser.Signal {
    return (this._onCollectedItem);
  }

  // ----------------------------------------------------------------------------------------------
  get onCollided(): Phaser.Signal {
    return (this._onCollided);
  }

  // ----------------------------------------------------------------------------------------------
  get speed(): number {
    return (this._speed);
  }
  
  // ----------------------------------------------------------------------------------------------
  set speed(value: number) {
    this._speed = Math.max(1, value);
    
    const rate = this.calcMoveRate();
    if (this._moveTimerEvent) {
      this._moveTimerEvent.delay = rate;
    }
  }
  
  // ----------------------------------------------------------------------------------------------
  start(): void {
    if (this._yallDoneDied) {
      return;
    }

    if (!this._moveTimerEvent) {
      const rate = this.calcMoveRate();
      this._moveTimerEvent = this.gameState.timer.loop(rate, this.onTimerMove, this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    this._moveTimerEvent = this.removeTimer(this._moveTimerEvent);
  }

  // -----------------------------------------------------------------------------------------------
  get visible(): boolean {
    return (this._segments[0].image.visible);
  }

  // -----------------------------------------------------------------------------------------------
  set visible(value: boolean) {
    this._segments.forEach((segment) => {
      segment.image.visible = value;
    });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private calcMoveRate(): number {
    const config = this.gameState.gameObject.snkConfig;
    let rate = config.baseSnakeMoveRateMs - config.snakeMoveRateDelta * this.speed;
    rate = Math.max(rate, config.snakeMoveRateDelta);
    this._speed = this._correctSpeed(rate);
    return (rate);
  }

  // ----------------------------------------------------------------------------------------------
  private checkCollisions(): boolean {
    this._checkCollisionsProposedItem();
    return (this._checkCollisionsProposedSelf() || this._checkCollisionsProposedBlocks());
  }

  // ----------------------------------------------------------------------------------------------
  private _checkCollisionsProposedBlocks(): boolean {
    const col = this._proposedPositions[0].x;
    const row = this._proposedPositions[0].y;
    const tile = this.gameState.level.getTile(col, row);
    if (tile && tile.isBlockActive) {
      this.onCollidedWithTile(tile);
      return (true);
    }
  }
  
  // ----------------------------------------------------------------------------------------------
  private _checkCollisionsProposedItem(): void {
    const col = this._proposedPositions[0].x;
    const row = this._proposedPositions[0].y;
    this.gameState.items.forEach((item) => {
      if (item.column === col && item.row === row) {
        this.onCollideWithItem(item);
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  private _checkCollisionsProposedSelf(): boolean {
    if (this.hasShield) {
      return (false); // I'm invincible! >:D
    }

    const MIN_LENGTH_TO_CHECK = 5;
    if (this.length < MIN_LENGTH_TO_CHECK) {
      // snake too short to collide with itself
      return (false);
    }

    const headCol = this._proposedPositions[0].x;
    const headRow = this._proposedPositions[0].y;
    const startIndex = MIN_LENGTH_TO_CHECK - 1;
    for (let segmentIndex = startIndex; segmentIndex < this.length; segmentIndex += 1) {
      const segCol = this._proposedPositions[segmentIndex].x;
      const segRow = this._proposedPositions[segmentIndex].y;
      if (segRow === headRow && segCol === headCol) {
        this.onCollidedWithSelf();
        return (true);
      }
    }

    return (false);
  }

  // ----------------------------------------------------------------------------------------------
  private _correctSpeed(rate: number): number {
    const config = this.gameState.gameObject.snkConfig;
    const speed = -(rate - config.baseSnakeMoveRateMs) / config.snakeMoveRateDelta;
    return (speed);
  }

  // ----------------------------------------------------------------------------------------------
  private createSegmentImage(segment: _ISnakeSegment, frame: string): void {
    const grid = this.gameState.grid;
    const x = grid.toCoor(segment.column);
    const y = grid.toCoor(segment.row);
    
    const animationFrames = Phaser.Animation.generateFrameNames(
      'snake-death-animation',
      0,
      11,
      '',
      4
    );

    const image = this.gameState.game.add.image(x, y, JsonKey.SNAKE_ATLAS, frame);
    image.animations.add('death', animationFrames);

    segment.image = image;
    this.gameState.boardGroup.add(segment.image);
  }

  // ----------------------------------------------------------------------------------------------
  private destroySegments():void {
    this._segments.forEach((segment) => {
      if (segment.image) {
        segment.image.destroy();
      }
    });

    this._segments = [];
  }

  // ----------------------------------------------------------------------------------------------
  private getSegmentImageFrame(index: number, length?: number): string {
    if (index === 0) {
      return (`snake-head-${this.direction}`);
    }
    
    length = length || this.length;
    if (index === length - 1) {
      // TODO (tail?)
      return ('snake-body');
    }

    return ('snake-body');
  }

  // ----------------------------------------------------------------------------------------------
  private grow(length: number): void {
    for (let index = 0; index < length; index += 1) {
      const frame = this.getSegmentImageFrame(index, length);
      const segment = this._segments[index];
      if (segment) {
        this._growExistingSegment(segment, frame);
      } else {
        this._growNewSegment(frame, index);
      }
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _growExistingSegment(segment: _ISnakeSegment, frameName: string): void {
    if (segment.image) {
      if (frameName !== segment.image.frameName) {
        segment.image.frameName = frameName;
      }
    } else {
      this.createSegmentImage(segment, frameName);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _growNewSegment(frame: string, index: number): _ISnakeSegment {
    const prevSegment = this._segments[index - 1];
    const position = new Phaser.Point(
      prevSegment ? prevSegment.column : 0,
      prevSegment ? prevSegment.row : 0,
    );

    const segment: _ISnakeSegment = {
      column: position.x,
      row: position.y,
    };

    this.createSegmentImage(segment, frame);
    
    this._proposedPositions[index] = position;
    this._segments[index] = segment;
    return (segment);
  }

  // ----------------------------------------------------------------------------------------------
  private initSegments(column: number, row: number, length: number): void {
    const headPosition = new Phaser.Point(column, row);
    this._proposedPositions = [ headPosition ];
    this._prevPositions = [];
    
    this._segments = [
      {
        column: headPosition.x,
        row: headPosition.y,
      },
    ];

    this.length = Math.max(1, length);
  }

  // ----------------------------------------------------------------------------------------------
  private onCollideWithItem(item: Item) {
    this._onCollectedItem.dispatch(item, this);
  }

  // ----------------------------------------------------------------------------------------------
  private onCollidedWithSelf(): void {
    this._onCollided.dispatch(<ISnakeCollideData>{
      collidedWithSelf: true,
    }, this);
  }
  
  // ----------------------------------------------------------------------------------------------
  private onCollidedWithTile(tile: Tile): void {
    this._onCollided.dispatch(<ISnakeCollideData>{
      collidedTile: tile,
    }, this);
  }
  
  // ----------------------------------------------------------------------------------------------
  private onDurationTimerComplete(): void {
    this._hasShield = false;
  }

  // ----------------------------------------------------------------------------------------------
  private onTimerMove(): void {
    this.updateMove();
  }

  // ----------------------------------------------------------------------------------------------
  private removeTimer(event: Phaser.TimerEvent): Phaser.TimerEvent {
    if (event) {
      event.timer.remove(event);
    }

    return (null);
  }

  // ----------------------------------------------------------------------------------------------
  private revertProposedPositions(): void {
    const length = this._prevPositions.length;
    for (let index = 0; index < length; index += 1) {
      let target = this._proposedPositions[index];
      if (!target) {
        target = new Phaser.Point();
        this._proposedPositions[index] = target;
      }
      
      target.copyFrom(this._prevPositions[index]);
    }
  }
  
  // ----------------------------------------------------------------------------------------------
  private savePrevPositions(): void {
    const length = this._proposedPositions.length;
    for (let index = 0; index < length; index += 1) {
      let target = this._prevPositions[index];
      if (!target) {
        target = new Phaser.Point();
        this._prevPositions[index] = target;
      }

      target.copyFrom(this._proposedPositions[index]);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private setHeadDirection(): void {
    const head = this._segments[0];
    const frameName = this.getSegmentImageFrame(0);
    if (head.image.frameName !== frameName) {
      head.image.frameName = frameName;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private shrink(length: number): void {
    for (let index = length; index < this.length; index += 1) {
      const segment = this._segments[index];
      if (!segment.image) {
        continue;
      }

      const anim = segment.image.play('death');
      anim.onComplete.addOnce(() => {
        segment.image.destroy();
      });
    }

    this._segments.length = length;
    this._proposedPositions.length = length;
  }

  // ----------------------------------------------------------------------------------------------
  private _updateDirection(): void {
    if (this._pendingDirection) {
      this.direction = this._pendingDirection;
      this._pendingDirection = null;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _updateHeadPosition(): void {
    const head = this._segments[0];
    if (!head) {
      return; // sanity check
    }
    
    head.column = this._proposedPositions[0].x;
    head.row = this._proposedPositions[0].y;

    this._updateSegmentImagePosition(head);
  }

  // -----------------------------------------------------------------------------------------------
  private _updatePositionWrapColumn(column: number): number {
    const minColumn = 0;
    const maxColumn = this.gameState.level.columns - 1;
    if (column < minColumn) {
      column = maxColumn;
    } else if (column > maxColumn) {
      column = minColumn;
    }
    return (column);
  }

  // -----------------------------------------------------------------------------------------------
  private _updatePositionWrapRow(row: number): number {
    const minRow = 0;
    const maxRow = this.gameState.level.rows - 1;
    if (row < minRow) {
      row = maxRow;
    } else if (row > maxRow) {
      row = minRow;
    }
    return (row);
  }

  // ----------------------------------------------------------------------------------------------
  private updateMove(): void {
    if (this._yallDoneDied) {
      return;
    }

    this._updateDirection();
    this._updateProposedPositions();
    
    if (this.checkCollisions()) {
      this.revertProposedPositions();
    } else {
      this._updateSegments();
      this._updateHeadPosition();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _updateProposedPositions(): void {
    const head = this._segments[0];
    if (!head) {
      return; // sanity check
    }
    
    this.savePrevPositions();

    const headSegment = this._segments[0];
    let column = headSegment.column;
    let row = headSegment.row;
    
    this._proposedPositions[0].set(column, row);

    if (this.direction == Direction.UP) {
      row -= 1;
    } else if (this.direction == Direction.DOWN) {
      row += 1;
    } else if (this.direction == Direction.LEFT) {
      column -= 1;
    } else if (this.direction == Direction.RIGHT) {
      column += 1;
    }

    column = this._updatePositionWrapColumn(column);
    row = this._updatePositionWrapRow(row);

    for (let segmentIndex = this.length - 1; segmentIndex > 0; segmentIndex -= 1) {
      const position = this._proposedPositions[segmentIndex];
      const prevPosition = this._proposedPositions[segmentIndex - 1];
      position.copyFrom(prevPosition);
    }

    this._proposedPositions[0].set(column, row);
  }

  // -----------------------------------------------------------------------------------------------
  private _updateSegmentImagePosition(segment: _ISnakeSegment): void {
    if (!segment.image) {
      return; //sanity check
    }

    const grid = this.gameState.grid;
    segment.image.x = grid.toCoor(segment.column);
    segment.image.y = grid.toCoor(segment.row);
  }

  // -----------------------------------------------------------------------------------------------
  private _updateSegments(): void {
    for (let segmentIndex = this.length - 1; segmentIndex > 0; segmentIndex -= 1) {
      const segment = this._segments[segmentIndex];
      const position = this._proposedPositions[segmentIndex];
      segment.column = position.x;
      segment.row = position.y;
      this._updateSegmentImagePosition(segment);
    }
  }
}
