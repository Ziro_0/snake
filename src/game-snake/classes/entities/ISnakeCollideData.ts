import Tile from '../levels/Tile';

interface ISnakeCollideData {
  collidedWithSelf?: boolean;
  collidedTile?: Tile;
}

export default ISnakeCollideData;
