import ItemSpawnLocation from './ItemSpawnLocation';

export default interface ILevelJson {
  speed: number;
  tiles: string[];
  itemSpawnLocations: ItemSpawnLocation[];
  snakeStartPosition: Phaser.Point;
  snakeStartDirection: string;
  itemSpawnRate: number;
  maxItems: number;
}
