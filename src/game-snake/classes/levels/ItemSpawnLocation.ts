import { getProperty } from '../../util/util';

export default class ItemSpawnLocation {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  x: number;
  y: number;
  width: number;
  height: number;
  weight: number;
  items: string[];

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(jData: any) {
    this.x = getProperty(jData, 'x', 0);
    this.y = getProperty(jData, 'y', 0);
    this.width = getProperty(jData, 'width', 1);
    this.height = getProperty(jData, 'height', 1);
    this.weight = getProperty(jData, 'weight', 1);
    this.items = getProperty(jData, 'items', []);
  }
}
