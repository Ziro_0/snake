import GameState from '../../states/GameState';
import { chooseWeights, getProperty } from '../../util/util';
import Direction from '../data/Direction';
import ILevelJson from './ILevelJson';
import ItemSpawnLocation from './ItemSpawnLocation';
import Tile from './Tile';
import TileFlasher from './TileFlasher';
import TilesData from './TilesData';

// ==============================================================================================
interface ITile {
  tile?: Tile;
  index?: number;
}

// ==============================================================================================
interface IPass {
  numItems?: number;
  time?: number;
}

export default class Level {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _tiles: ITile[][];
  private _itemSpawnLocations: ItemSpawnLocation[];
  
  private _gameState: GameState;
  private _tilesData: TilesData;
  private _tileFlasher: TileFlasher;
  private _snakeStartDirection: Direction;
  
  private _snakeStartPosition: Phaser.Point;
  private _tilesGroup: Phaser.Group;

  private _cellSize: number;
  private _passItems: number;
  private _passTime: number;
  private _itemSpawnRate: number;
  private _maxItems: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // -----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, cellSize: number) {
    this._gameState = gameState;
    this._tilesGroup = this._gameState.add.group(this._gameState.boardGroup);
    this._tilesData = new TilesData(this._gameState.game);
    this._cellSize = cellSize;
    this._tiles = [];
    this._itemSpawnLocations = [];
    this._snakeStartPosition = new Phaser.Point();
    this._tileFlasher = new TileFlasher(gameState);
  }

  // -----------------------------------------------------------------------------------------------
  // public
  // -----------------------------------------------------------------------------------------------

  // -----------------------------------------------------------------------------------------------
  get columns(): number {
    const column = this._tiles[0];
    return (column ? column.length : 0);
  }

  // -----------------------------------------------------------------------------------------------
  destroy(): void {
    if (this._tilesGroup) {
      this._tilesGroup.destroy();
      this._tilesGroup = null;
    }

    this._tileFlasher.destroy();

    this._tiles = null;
    this._gameState = null;
  }
  
  // -----------------------------------------------------------------------------------------------
  getRandomItemSpawnLocation(): ItemSpawnLocation {
    const weights = this.itemSpawnLocations.map(loc => loc.weight);
    const index = chooseWeights(weights);
    return (this.itemSpawnLocations[index]);
  }

  // -----------------------------------------------------------------------------------------------
  getTile(column: number, row: number): Tile {
    const tileRows = this._tiles[row]
    if (!tileRows) {
      return (null);
    }

    const tileData = tileRows[column];
    return (tileData ? tileData.tile : null)
  }

  // -----------------------------------------------------------------------------------------------
  get group(): Phaser.Group {
    return (this._tilesGroup);
  }

  // -----------------------------------------------------------------------------------------------
  get itemSpawnLocations(): ItemSpawnLocation[] {
    return (this._itemSpawnLocations);
  }

  // -----------------------------------------------------------------------------------------------
  get itemSpawnRate(): number {
    return (this._itemSpawnRate);
  }

  // -----------------------------------------------------------------------------------------------
  get length(): number {
    const keys = this._gameState.game.cache.getKeys(Phaser.Cache.JSON);
    const regex = /level\d/;
    return (keys.filter(key => regex.test(key)).length);
  }

  // -----------------------------------------------------------------------------------------------
  load(level: number, group?: Phaser.Group): boolean {
    const data = this.getLevelsJson(level);
    if (!data) {
      console.warn(`No level json data found for level ${level}.`);
      return (true);
    }

    this._itemSpawnLocations = Level.ParseItemSpawnLocations(data.itemSpawnLocations);
    this._snakeStartPosition = data.snakeStartPosition || new Phaser.Point();
    this._maxItems = data.maxItems || 1;
    this.parsePass(data);
    this._snakeStartDirection = data.snakeStartDirection as Direction || Direction.UP;
    this._itemSpawnRate = data.itemSpawnRate || 0;

    group = group || this._tilesGroup;
    if (!group) {
      this._tilesGroup = this._gameState.add.group();
    }
    
    this.initTiles(data.tiles);
    
    return (true);
  }

  // -----------------------------------------------------------------------------------------------
  get maxItems(): number {
    return (this._maxItems);
  }

  // -----------------------------------------------------------------------------------------------
  get passItems(): number {
    return (this._passItems);
  }

  // -----------------------------------------------------------------------------------------------
  get passTime(): number {
    return (this._passTime);
  }

  // -----------------------------------------------------------------------------------------------
  get rows(): number {
    return (this._tiles.length);
  }

  // -----------------------------------------------------------------------------------------------
  get snakeStartDirection(): Direction {
    return (this._snakeStartDirection);
  }

  // -----------------------------------------------------------------------------------------------
  get snakeStartPosition(): Phaser.Point {
    return (this._snakeStartPosition);
  }

  // -----------------------------------------------------------------------------------------------
  get tileFlasher(): TileFlasher {
    return (this._tileFlasher);
  } 

  // -----------------------------------------------------------------------------------------------
  // private
  // -----------------------------------------------------------------------------------------------

  // -----------------------------------------------------------------------------------------------
  private destroyTiles(): void {
    this._tiles.forEach((cellRow) => {
      cellRow.forEach((iTile) => {
        if (iTile.tile) {
          iTile.tile.destroy();
        }
      });
    });

    this._tiles = [];
  }
  
  // -----------------------------------------------------------------------------------------------
  private getLevelsJson(levelIndex: number): ILevelJson {
    const key = `level${levelIndex}`;
    const json = this._gameState.cache.getJSON(key);
    return (json as ILevelJson);
  }

  // -----------------------------------------------------------------------------------------------
  private initTiles(data: string[], x = 0, y = 0): void {
    this.destroyTiles();
    this._tiles = Level.ParseTiles(data);

    let yc = y;
    let row = 0;
    this._tiles.forEach((cellRow) => {
      let xc = x;
      let col = 0;
      cellRow.forEach((iTile) => {
        const tileJsonData = this._tilesData.data[iTile.index];
        iTile.tile = new Tile(this._gameState, xc, yc, col, row, tileJsonData);
        
        // KLUDGE ALERT!!
        // Kludge to address the horizontal lines - not sure where they're coming from...
        // It's not due to the texture atlas; plenty of padding in between individual images
        // is already there. I already tried using individual (non-atlas) spirtes, and
        // the issue is stll there.
        
        // I've also made sure the tile images are 100% opaque, and that
        // there's no pixel artifacts/discoloration along the edges of the tile.

        // For some reason, this glitch only appears as horizontal lines, vertically,
        // everything looks ok.

        // FYI: More about kludges:
        // https://en.wikipedia.org/wiki/Kludge
        // :-)
        iTile.tile.height += 1;

        this._tilesGroup.add(iTile.tile);
        xc += this._cellSize;
        col += 1;
      });

      yc += this._cellSize;
      row += 1;
    });
  }

  // -----------------------------------------------------------------------------------------------
  private parsePass(data: ILevelJson): void {
    const pass: IPass = getProperty(data, 'pass', {});
    this._passItems = pass.numItems;
    this._passTime = pass.time;
  }

  // -----------------------------------------------------------------------------------------------
  private static ParseTiles(jData: string[]): ITile[][] {
    const tileRows: ITile[][] = [];

    jData.forEach((jRow: string) => {
      const tileRow: ITile[] = [];
      for (let idx = 0; idx < jRow.length; idx += 1) {
        const char = jRow.charAt(idx);
        const tileIndex = Number.parseInt(char);
        tileRow.push({
          index: tileIndex,
        });
      }

      tileRows.push(tileRow);
    });

    return (tileRows);
  }

  // -----------------------------------------------------------------------------------------------
  private static ParseItemSpawnLocations(jData: object[]): ItemSpawnLocation[] {
    const locations: ItemSpawnLocation[] = [];

    jData.forEach((jUnit) => {
      locations.push(new ItemSpawnLocation(jUnit));
    });

    return (locations);
  }
}
