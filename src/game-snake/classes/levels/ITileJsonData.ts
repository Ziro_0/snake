export default interface ITileJsonData {
  imageFrame: string;
  tileIndex: number;
  isBlock: boolean;
  delay: number;
  inactiveImageFrame: string;
}
