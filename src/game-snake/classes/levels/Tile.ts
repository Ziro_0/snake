import { transpileModule } from 'typescript';
import GameState from '../../states/GameState';
import { getProperty, irandomRange, removeTimer } from '../../util/util';
import JsonKey from '../data/JsonKey';
import Item from '../entities/Item';
import ITileJsonData from './ITileJsonData';

export default class Tile extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private gameState: GameState;

  private _tileData: ITileJsonData;

  private presentTimerEvent: Phaser.TimerEvent;

  private _col: number;
  private _row: number;

  private _isBlock: boolean;
  
  private _isActive: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, x: number, y: number, col: number, row: number,
    data: ITileJsonData) {
    super(gameState.game, x, y, JsonKey.SNAKE_ATLAS);
    
    this.gameState = gameState;
    this._col = col;
    this._row = row;
    this._tileData = data;
    this._isBlock = getProperty(data, 'isBlock', false);
    this._isActive = true;

    this.showActiveImage();
    this.initDelay();
  }

  // ----------------------------------------------------------------------------------------------
  checkItemCollisions(): void {
    if (!this.gameState) {
      return;
    }

    const itemsToDestroy: Item[] = [];

    this.gameState.items.forEach((item) => {
      if (this.collidesWithItem(item)) {
        itemsToDestroy.push(item);
      }
    });

    itemsToDestroy.forEach((item) => {
      item.destroy();
      this.gameState.itemSpawner.spawn();
    });
  }

  // ----------------------------------------------------------------------------------------------
  get column(): number {
    return (this._col);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this.presentTimerEvent = removeTimer(this.presentTimerEvent);

    this._tileData = null;

    if (this.gameState) {
      this.gameState.level.tileFlasher.remove(this);
      this.gameState = null;
    }

    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get isBlock(): boolean {
    return (this._isBlock);
  }

  // ----------------------------------------------------------------------------------------------
  get isBlockActive(): boolean {
    return (this._isBlock && this._isActive);
  }

  // ----------------------------------------------------------------------------------------------
  get row(): number {
    return (this._row);
  }

  // ----------------------------------------------------------------------------------------------
  shatter(): void {
    if (!this._isBlock || !this._isActive) {
      return;
    }

    const MIN_DELAY_SECS = 5.0;
    const MAX_DELAY_SECS = 15.0;
    const delay = irandomRange(MIN_DELAY_SECS, MAX_DELAY_SECS) * 1000;
    this.initDelay(delay);
  }

  // ----------------------------------------------------------------------------------------------
  showActiveImage(): void {
    if (this.tileData) {
      this.frameName = this.tileData.imageFrame;
    }
  }

  // ----------------------------------------------------------------------------------------------
  showInactiveImage(): void {
    if (this.tileData) {
      this.frameName = this.tileData.inactiveImageFrame;
    }
  }

  // ----------------------------------------------------------------------------------------------
  get tileData(): ITileJsonData {
    return (this._tileData);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private collidesWithItem(item: Item): boolean {
    return (this._col === item.column && this._row === item.row);
  }

  // ----------------------------------------------------------------------------------------------
  private initDelay(delay?: number): void {
    if (!this._tileData) {
      return;
    }

    if (delay === undefined) {
      delay = getProperty(this._tileData, 'delay', -1);
    }

    if (delay < 0) {
      return;
    }

    this._isActive = false;
    this.showInactiveImage();

    this.presentTimerEvent = this.gameState.timer.add(delay, () => {
      this.present();
    }, this);
  }

  // ----------------------------------------------------------------------------------------------
  private present(): void {
    this.gameState.level.tileFlasher.add(this);

    this.presentTimerEvent = this.gameState.timer.add(2000, () => {
      this.presentTimerEvent = null;
      this.showActiveImage();
      this._isActive = true;
      this.checkItemCollisions();
      this.gameState.level.tileFlasher.remove(this);
    }, this);
  }
}
