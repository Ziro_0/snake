import { getProperty } from '../../util/util';
import ItemEffect from '../data/ItemEffect';

export default class ItemData {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  effects: ItemEffect[];
  sound: string[];
  key: string;
  imageFrame: string;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(jData: any) {
    this.key = getProperty(jData, 'key', '');
    this.imageFrame = getProperty(jData, 'imageFrame', '');
    this.effects = getProperty(jData, 'effects', []);
    this.sound = getProperty(jData, 'sound', []);
  }
}
