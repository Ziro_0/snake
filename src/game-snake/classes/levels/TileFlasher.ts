import GameState from '../../states/GameState';
import { removeTimer } from '../../util/util';
import Tile from './Tile';

export default class TileFlasher {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  gameState: GameState;
  
  private _tiles: Set<Tile>

  private timerEvent: Phaser.TimerEvent;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState) {
    this.gameState = gameState;
    this._tiles = new Set();
  }

  // ----------------------------------------------------------------------------------------------
  add(tile: Tile): void {
    if (tile) {
      this._tiles.add(tile);
    }

    this.startTimer();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.stopTimer();
    this._tiles.clear();
  }

  // ----------------------------------------------------------------------------------------------
  remove(tile: Tile): void {
    this._tiles.delete(tile);
    
    if (this._tiles.size === 0) {
      this.stopTimer();
    }
  }


  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private startTimer(): void {
    if (this.timerEvent) {
      return;
    }
    
    let showActiveTileFrame = false;

    this.timerEvent = this.gameState.timer.loop(50, () => {
      this._tiles.forEach((tile) => {
        if (showActiveTileFrame) {
          tile.showActiveImage();
        } else {
          tile.showInactiveImage();
        }
      });

      showActiveTileFrame = !showActiveTileFrame;
    }, this);
  }

  // ----------------------------------------------------------------------------------------------
  private stopTimer(): void {
    this.timerEvent = removeTimer(this.timerEvent);
  }
}
