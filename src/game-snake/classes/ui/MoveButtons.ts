import { IPoint } from '../../ISnakeGameConfig';
import GameState from '../../states/GameState';
import { getProperty } from '../../util/util';
import JsonKey from '../data/JsonKey';

// ================================================================================================
export type IMoveButtonCallback = (moveButtons: MoveButtons) => void;

// ================================================================================================
export default class MoveButtons {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private gameState: GameState;

  private _group: Phaser.Group;
  
  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // -----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, upCallback: IMoveButtonCallback,
    downCallback: IMoveButtonCallback, leftCallback: IMoveButtonCallback,
    rightCallback: IMoveButtonCallback, callbackContext: any) {
    this.gameState = gameState;
    
    this._group = this.gameState.game.add.group(gameState.uiGroup);
    
    this.setPosition();
    this.setScale();
    this.createButtons(upCallback, downCallback, leftCallback, rightCallback, callbackContext);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // -----------------------------------------------------------------------------------------------
  private createButton(property: string, xDefault: number, yDefault: number, id: string,
  callback: IMoveButtonCallback, callbackContext: any): Phaser.Button {
    const moveButtons = this.gameState.gameObject.snkConfig.moveButtons;
    
    const x = getProperty(moveButtons, `${property}.x`, xDefault);
    const y = getProperty(moveButtons, `${property}.y`, yDefault);
    
    const button = this.gameState.game.add.button(
      x,
      y,
      JsonKey.SNAKE_ATLAS,
      callback,
      callbackContext,
      `button-${id}-over`,
      `button-${id}-norm`,
      `button-${id}-over`,
      `button-${id}-norm`,
      this._group);
    
    return (button);
  }

  // -----------------------------------------------------------------------------------------------
  private createButtons(upCallback: IMoveButtonCallback, downCallback: IMoveButtonCallback,
  leftCallback: IMoveButtonCallback, rightCallback: IMoveButtonCallback,
  callbackContext: any): void {
    this.createButton('up', -16, -32, 'up', upCallback, callbackContext);
    this.createButton('down', -16, 0, 'down', downCallback, callbackContext);
    this.createButton('left', -48, -16, 'left', leftCallback, callbackContext);
    this.createButton('right', 16, -16, 'right', rightCallback, callbackContext);
  }

  // -----------------------------------------------------------------------------------------------
  private setPosition(): void {
    const config = this.gameState.gameObject.snkConfig.moveButtons;
    const Y_DEFAULT = 272;
    const X_DEFAULT = 786;
    this._group.x = getProperty(config, 'position.x', Y_DEFAULT);
    this._group.y = getProperty(config, 'position.y', X_DEFAULT);
  }

  // -----------------------------------------------------------------------------------------------
  private setScale(): void {
    const config = this.gameState.gameObject.snkConfig.moveButtons;
    const SCALE_DEFAULT = 2.0;
    this._group.scale.set(getProperty(config, 'scale', SCALE_DEFAULT));
  }
}