import GameState from '../../states/GameState';
import JsonKey from '../data/JsonKey';

// ================================================================================================
export default class LifeBar {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _group: Phaser.Group;
  
  private _frame: Phaser.Image;
  private _fill: Phaser.Image;

  private _value: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // -----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, x: number, y: number) {
    const game = gameState.game;
    
    this._group = game.add.group(gameState.uiGroup);
    this._group.x = x;
    this._group.y = y;

    this._frame = game.add.image(0, 0, JsonKey.SNAKE_ATLAS, 'life-bar-frame', this._group);
    
    const OFFSET = 4
    this._fill = game.add.image(OFFSET, OFFSET, JsonKey.SNAKE_ATLAS, 'life-bar-fill', this._group);

    this._value = 1.0;
  }

  // -----------------------------------------------------------------------------------------------
  destroy(): void {
    this._group.destroy();
  }

  // -----------------------------------------------------------------------------------------------
  get value(): number {
    return (this._value);
  }

  // -----------------------------------------------------------------------------------------------
  set value(v: number) {
    this._value = Math.min(1.0, Math.max(0, v));
    this._fill.scale.x = this._value;
  }
}
