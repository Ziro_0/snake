enum Listeners {
  ADD_POINT = 'add-point',
  LIVES_LOST = 'lives-lost',
  SNAKE_SHATTER_BLOCK = 'snake-shatter-block',
  SNAKE_HIT_BLOCK = 'snake-hit-block',
  SNAKE_HIT_SELF = 'snake-hit-self',
  READY = 'ready',
}

export default Listeners;
