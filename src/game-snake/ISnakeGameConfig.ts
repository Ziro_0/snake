/**
 * Defines a 2D point.
 */
export interface IPoint {
  /**
   * The x coordinate of the point.
   */
  x?: number;
  
  /**
   * The y coordinate of the point.
   */
  y: number;
}

// ================================================================================================
/**
 * Defines the four move buttons as well as the scale of the group that contains all four
 * buttons. The scale is applied to the group, not the individual buttons. When
 * re-positioning and/or re-sizing the buttons, the settings should be applied to the
 * original (1.0) scale.
 * 
 * See the `ISnakeConfigMoveButtons` file for more details.
 */
 export interface ISnakeConfigMoveButtons {
  /**
   * Defines the position of the move buttons group
   */
  position: IPoint;

  /**
   * The scale of the group that contains the move buttons. A value of 1.0 = original scale,
   * 2.0 = double, 0.5 = half, etc.
   */
  scale?: number;

  /**
   * The position of the 'up' move button.
   */
  up?: IPoint;

  /**
   * The position of the 'down' move button.
   */
  down?: IPoint;

  /**
   * The position of the 'left' move button.
   */
  left?: IPoint;

  /**
   * The position of the 'right' move button.
   */
  right?: IPoint;
}

// ================================================================================================
export default interface ISnakeGameConfig {
  /**
   * Rate, in ms, at which the snake updates. Slower rates mean the snake updates more frequently
   * (faster movement).
   */
  baseSnakeMoveRateMs: number,

  /**
   * An array of sound keys that play when the snake shatters a block. One is chosen at random
   * from the array. To only have a single sound, only put one key in the array,
   */
  blockShatterSounds: string[],
  
  /**
   * Starting length of the snake.
   */
  defaultSnakeLength: number,
  
  /**
   * Starting speed of the snake. Higher numbers means faster movement. Works in conjunction
   * with `baseSnakeMoveRateMs`.
   */
  defaultSnakeSpeed: number,
  
  /**
   * Duration, in ms, that an item will stay on the screen before proceeding to the warning phase
   * (when it's about to disappear).
   */
  itemDuration: number,
  
  /**
   * Duration, in ms, of the warning phase of an item. When an item is about to disappear, it
   * enters what I'm calling the "warning phase". The item will flash during this time. When
   * the warning time expires, the item disappears.
   */
  itemWarningDuration: number,
  
  /**
   * Max length of the snake.
   */
  maxSnakeLength: number,

  /**
   * Defines the four move buttons as well as the scale of the group that contains all four
   * buttons. The scale is applied to the group, not the individual buttons. When
   * re-positioning and/or re-sizing the buttons, the settings should be applied to the
   * original (1.0) scale.
   */
  moveButtons: ISnakeConfigMoveButtons;

  /**
   * Determines if a collision will instantly kill the snake. Otherwise, each collision will
   * only remove the tail segment of the snake. If only the head is left, a collision will then
   * kill the snake.
   */
  oneHitDefCollisions: boolean;
  
  /**
   * Rate, in ms, at which the snake's speed changes, when it collects speed-based items.
   */
  snakeMoveRateDelta: number;
  
  /**
   * Duration, in ms, of the shield power-up.
   */
  shieldDuration: number;
  
  /**
   * Duration, in ms, of the shield's warning phase. Similar to an item's warning phase, except
   * this is applied to the shield power-up, used on the snake.
   */
  shieldWarningDuration: number;
  
  /**
   * An array of sound keys that play upon the snake's def (!!) One is chosen at random from
   * the array. To only have a single sound, only put one key in the array,
   */
  snakeDefSounds: string[];
  
  /**
   * An array of sound keys that play when the snake is hit and loses a segment. One is chosen
   * at random from the array. To only have a single sound, only put one key in the array,
   */
  snakeHitSounds: string[];

  /**
   * Starting level of the game. Currently only 1 level is supported, so this should be set to `0`.
   */
  startLevel: number;

  /**
   * Duration, in ms, of the flash effect of the warning phase.
   */
  warningFlashRateMs: number;
}
